# Gitlab Pomodoro

Ce dossier vise à contenir la documentation de Gitlab Pomodoro,
un utilitaire permettant de :
* se connecter à une instance Gitlab
* récupérer les projets / issues
* lancer des Pomodoro sur ces différentes issues
* enregistrer les temps passés selon le temps Pomodoro

## Get the access token

I propose you two methods to get your access token.

In both cases, you'll have to set the gitlab url to explain the webapp
where to connect and get your projects/issues.

By default, the gitlab instance is `https://gitlab.com`.

### First method: create your personal access token

### Second method: connect you on your gitlab instance
