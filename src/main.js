import Vue from 'vue'

import './plugins/vuetify'

import App from './App.vue'
import router from './router'
import store from './store/'

import API from './services/api'

import './filters/'

const { token, api } = store.getters['settings/currentInstance']
API.init(token, api)

// Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
}).$mount('#app')
