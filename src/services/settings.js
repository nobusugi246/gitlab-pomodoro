const PRIVATE_TOKEN_KEY = 'privateToken'
const GITLAB_URL_KEY = 'gitlabURL'

export const setSettings = function (token, url) {
  localStorage.setItem(PRIVATE_TOKEN_KEY, token)
  localStorage.setItem(GITLAB_URL_KEY, url)
}

export const getSettings = function () {
  let token = localStorage.getItem(PRIVATE_TOKEN_KEY)
  let url = localStorage.getItem(GITLAB_URL_KEY)
  return { token, url }
}
