# Gitlab Pomodoro

This project is about creating a [ tray icon | web extension | website ]
with pomodoro philosophy, allowing user to
* select an issue on a gitlab project (tracked issue)
* start a work period of 25mn (for example, but customizable)
* add a `/spend 25mn` to the selected issue when the timer is finished
* export time stats for a project in a CSV file (and maybe several projects too)

## How to start it ?

Two paths for using it : node.js only, and web browser.

### Node.js

First, you have to config your gitlab access / project.

* copy/paste the `config/example.config.js` in a `config/config.js`
* generate an access token through gitlab settings 
https://gitlab.com/profile/personal_access_tokens (give scope api access)
* copy/paste the access token in the file `config/config.js`
* find the url of your project (eg https://gitlab.com/mdartic/gitlab-pomodoro) 
and his id (eg https://gitlab.com/mdartic/gitlab-pomodoro/edit is 2995486)

Then run
```
npm install

npm run start-export

```

You'll get a `csv` in the project directory containing a table like this :

projectId|iid|title|state|labels|timeSpent(hours)|url|
---|---|---|---|---|---|---|

### Web browser

The web part is written in Vue JS, so for the moment it will be a website.

```
npm install

npm start

```

A new web browser will open and you'll see the early website.

You'll have to go to the settings page and generate as said in 
the node.js part a private token.

You will be able to export time data for all the project
you are a member.

## What is in progress

Currently, my work is to get projects, issues and time stats
from gitlab API, v4.

If you want to discuss about the project, 
please open an issue.

You can check the [board](https://gitlab.com/mdartic/gitlab-pomodoro/boards)
to see which issues are in progress.

### Roadmap

Done
* get the issues affected to a project => ok
* get the time stats for an issue => ok
* export a file containing, for a project, issues, assignee and time stats => ok

To do
* get the issues of the user, for each project he has access (membership = true) #8
* add ui to visualize projects / issues / times #2 #6
* integrate a timer #3 
* allow the user to track an issue #4
* add the time spent to the issue tracked #5
* allow user to change time stats #7
